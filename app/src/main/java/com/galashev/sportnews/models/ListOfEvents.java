package com.galashev.sportnews.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListOfEvents {

        @SerializedName("events")
        private List<Event> events = null;

        public List<Event> getEvents() {
            return events;
        }

        public void setEvents(List<Event> events) {
            this.events = events;
        }

}
