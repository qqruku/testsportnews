package com.galashev.sportnews.models;

import com.google.gson.annotations.SerializedName;

public class Event {

    @SerializedName("title")
    private String mTitle;

    @SerializedName("coefficient")
    private String mCoef;

    @SerializedName("time")
    private String mTime;

    @SerializedName("place")
    private String mPlace;

    @SerializedName("preview")
    private String mPreview;

    @SerializedName("article")
    private String mArticle;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getCoef() {
        return mCoef;
    }

    public void setCoef(String mCoef) {
        this.mCoef = mCoef;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String mTime) {
        this.mTime = mTime;
    }

    public String getPlace() {
        return mPlace;
    }

    public void setPlace(String mPlace) {
        this.mPlace = mPlace;
    }

    public String getPreview() {
        return mPreview;
    }

    public void setPreview(String mPreview) {
        this.mPreview = mPreview;
    }

    public String getArticle() {
        return mArticle;
    }

    public void setArticle(String mArticle) {
        this.mArticle = mArticle;
    }
}
