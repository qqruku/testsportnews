package com.galashev.sportnews.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Article implements Serializable{

    @SerializedName("header")
    private String header;

    @SerializedName("text")
    private String text;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
