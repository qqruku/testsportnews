package com.galashev.sportnews;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.galashev.sportnews.models.Article;
import com.galashev.sportnews.models.EventDetail;

import java.util.ArrayList;
import java.util.List;

public class DetailNewsActivity extends AppCompatActivity {

    private RecyclerView mRecyclerDetail;
    private TextView tvTeams;
    private TextView tvTime;
    private TextView tvTournament;
    private TextView tvPlace;
    @NonNull
    private final ArticleAdapter mArticleAdapter = new ArticleAdapter();
    @NonNull
    private List<Article> mListOfArticles = new ArrayList<>();
    private EventDetail mEventDetail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_news);

        mRecyclerDetail = findViewById(R.id.recycler_detail);
        mRecyclerDetail.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerDetail.setAdapter(mArticleAdapter);

        tvTeams = findViewById(R.id.tv_teams);
        tvTime = findViewById(R.id.tv_time);
        tvTournament = findViewById(R.id.tv_tournament);
        tvPlace = findViewById(R.id.tv_place);

        Intent intent = getIntent();
        mEventDetail = (EventDetail) intent.getSerializableExtra("EVENT_DETAIL");
        mArticleAdapter.addData(mEventDetail.getArticle());

        tvTeams.setText(mEventDetail.getTeam1() + " - " + mEventDetail.getTeam2());
        tvTime.setText(mEventDetail.getTime());
        tvTournament.setText(mEventDetail.getTournament());
        tvPlace.setText(mEventDetail.getPlace());
    }





    public class ArticleHolder extends RecyclerView.ViewHolder{

        private TextView tvHeader;
        private TextView tvText;

        public ArticleHolder(View itemView) {
            super(itemView);
            tvHeader = itemView.findViewById(R.id.tv_header);
            tvText = itemView.findViewById(R.id.tv_text);
        }
    }
    public class ArticleAdapter extends RecyclerView.Adapter{

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(DetailNewsActivity.this);
            View v = layoutInflater.inflate(R.layout.list_item_detail, parent, false);
            return new ArticleHolder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((ArticleHolder) holder).tvHeader.setText(mListOfArticles.get(position).getHeader());
            ((ArticleHolder) holder).tvText.setText(mListOfArticles.get(position).getText());
        }

        @Override
        public int getItemCount() {
            return mListOfArticles.size();
        }

        public void addData(List<Article> data){

            mListOfArticles.addAll(data);
            notifyDataSetChanged();
        }
    }
}
