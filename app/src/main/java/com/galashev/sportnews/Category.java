package com.galashev.sportnews;

public enum Category {
    FOOTBALL {
        @Override
        public String toString() {
            return "football";
        }
    },

    HOCKEY {
        @Override
        public String toString() {
            return "hockey";
        }
    },

    TENNIS {
        @Override
        public String toString() {
            return "tennis";
        }
    },
    BASKETBALL {
        @Override
        public String toString() {
            return "basketball";
        }
    }, VOLLEYBALL {
        @Override
        public String toString() {
            return "volleyball";
        }
    }, CYBERSPORT {
        @Override
        public String toString() {
            return "cybersport";
        }
    }
}
