package com.galashev.sportnews;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiUtils {

    public static final String SERVER_URL = "http://mikonatoruri.win/";

    public static NewsApi getApi() {
        Gson gson = new GsonBuilder().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        NewsApi newsApi = retrofit.create(NewsApi.class);
        return newsApi;

    }
}
