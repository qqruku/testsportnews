package com.galashev.sportnews;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class ListOfNewsActivity extends AppCompatActivity {


    private ViewPager mViewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_news);
        mViewPager = findViewById(R.id.view_pager);
        mViewPager.setAdapter(new TabFragmentPagerAdapter(getSupportFragmentManager()));
        mViewPager.setCurrentItem(0);
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);
    }

    class TabFragmentPagerAdapter extends FragmentPagerAdapter {

        final int PAGE_COUNT = 6;
        private Category tabTitles[] = Category.values();

        public TabFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override public Fragment getItem(int position) {
            Category[] cats = Category.values();
            Category cat = cats[position];
            return NewsFragment.newInstance(cat);
        }

        @Override public CharSequence getPageTitle(int position) {
            return tabTitles[position].toString();
        }


    }
}
