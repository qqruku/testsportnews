package com.galashev.sportnews;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.galashev.sportnews.models.Event;
import com.galashev.sportnews.models.EventDetail;
import com.galashev.sportnews.models.ListOfEvents;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mRefresher;
    @NonNull
    private final NewsAdapter mNewsAdapter = new NewsAdapter();
    private RecyclerView mRecyclerView;
    @NonNull
    private final List<Event> mListOfNews = new ArrayList<>();
    public static final String CATEGORY = "CATEGORY";
    private String mCategory;
    private boolean isDataLoaded;


    public static NewsFragment newInstance(Category category) {
        Bundle args = new Bundle();
        args.putSerializable(CATEGORY, category);
        NewsFragment fragment = new NewsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_tab, container, false);
        mRefresher = view.findViewById(R.id.refresher);
        mRefresher.setOnRefreshListener(this);
        mRecyclerView = view.findViewById(R.id.recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mNewsAdapter);
        mCategory = getArguments().getSerializable(CATEGORY).toString();
        if (!isDataLoaded) {
            onRefresh();
        }
        return view;
    }

    @Override
    public void onRefresh() {
        mRefresher.setRefreshing(true);
        ApiUtils.getApi()
                .getListOfEvents(mCategory)
                .enqueue(new Callback<ListOfEvents>() {
                    @Override
                    public void onResponse(Call<ListOfEvents> call, Response<ListOfEvents> response) {
                        Log.d("Retrofit", "onResponse: " + response.toString());
                        mRefresher.setRefreshing(false);

                        switch (response.code()) {

                            case 200:
                                isDataLoaded = true;
                                mNewsAdapter.addData(response.body().getEvents(), true);
                                break;

                            case 400:
                                showToast(getString(R.string.bad_request));
                                break;

                            case 403:
                                showToast(getString(R.string.forbidden_request));
                                break;

                            case 404:
                                showToast(getString(R.string.not_found_request));
                                break;

                            case 408:
                                showToast(getString(R.string.timeout_request));
                                break;
                        }
                        isDataLoaded = true;
                        mNewsAdapter.addData(response.body().getEvents(), true);
                    }

                    @Override
                    public void onFailure(Call<ListOfEvents> call, Throwable t) {
                        Log.d("Retrofit", "onFailure: " + t.getClass().getName());
                        showToast(getString(R.string.network_error));
                        mRefresher.setRefreshing(false);
                    }
                });
    }

    public class NewsHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private TextView tvPreview;

        public NewsHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_event_title);
            tvPreview = itemView.findViewById(R.id.tv_preview);
        }
    }

    public class NewsAdapter extends RecyclerView.Adapter {


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View v = layoutInflater.inflate(R.layout.list_item_news, parent, false);
            return new NewsHolder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            ((NewsHolder) holder).tvTitle.setText(mListOfNews.get(position).getTitle());
            ((NewsHolder) holder).tvPreview.setText(mListOfNews.get(position).getPreview());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRefresher.setRefreshing(true);
                    ApiUtils.getApi()
                            .getEventDetail(mListOfNews.get(position).getArticle())
                            .enqueue(new Callback<EventDetail>() {
                                @Override
                                public void onResponse(Call<EventDetail> call, Response<EventDetail> response) {
                                    Log.d("Retrofit", "onResponse: " + response.toString());

                                    switch (response.code()) {

                                        case 200:
                                            Intent intent = new Intent(getActivity(), DetailNewsActivity.class);
                                            intent.putExtra("EVENT_DETAIL", response.body());
                                            startActivity(intent);
                                            break;

                                        case 400:
                                            showToast(getString(R.string.bad_request));
                                            break;

                                        case 403:
                                            showToast(getString(R.string.forbidden_request));
                                            break;

                                        case 404:
                                            showToast(getString(R.string.not_found_request));
                                            break;

                                        case 408:
                                            showToast(getString(R.string.timeout_request));
                                            break;
                                    }

                                    mRefresher.setRefreshing(false);
                                }

                                @Override
                                public void onFailure(Call<EventDetail> call, Throwable t) {
                                    Log.d("Retrofit", "onFailure: " + t.getClass().getName());
                                    showToast(getString(R.string.network_error));
                                    mRefresher.setRefreshing(false);
                                }
                            });
                }
            });
        }

        @Override
        public int getItemCount() {
            return mListOfNews.size();
        }

        public void addData(List<Event> data, boolean isRefreshed) {
            if (isRefreshed) {
                mListOfNews.clear();
            }
            mListOfNews.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
