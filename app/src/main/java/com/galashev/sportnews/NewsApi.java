package com.galashev.sportnews;

import com.galashev.sportnews.models.EventDetail;
import com.galashev.sportnews.models.ListOfEvents;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {

    @GET("list.php")
    Call<ListOfEvents> getListOfEvents(@Query("category") String category);

    @GET("post.php")
    Call<EventDetail> getEventDetail(@Query("article") String article);

}
